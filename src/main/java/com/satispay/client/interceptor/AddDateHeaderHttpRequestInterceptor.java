package com.satispay.client.interceptor;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddDateHeaderHttpRequestInterceptor implements ClientHttpRequestInterceptor
{

  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
    ClientHttpRequestExecution execution) throws IOException
  {
    request.getHeaders().add(HttpHeaders.DATE, currentDate());
    return execution.execute(request, body);
  }

  private String currentDate()
  {
    return new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z").format(new Date());
  }
}
