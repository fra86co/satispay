package com.satispay.client.interceptor;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class AddDigestHeaderHttpRequestInterceptor implements ClientHttpRequestInterceptor
{
  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
    ClientHttpRequestExecution execution) throws IOException
  {
    try
    {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      final byte[] digest = messageDigest.digest(body);
      final byte[] encodeDigest = Base64.getEncoder().encode(digest);

      request.getHeaders().add("Digest", new String(encodeDigest, StandardCharsets.UTF_8));
      return execution.execute(request, body);
    }
    catch (NoSuchAlgorithmException e)
    {
      throw new RuntimeException("Algorithm not found",e);
    }
  }
}
