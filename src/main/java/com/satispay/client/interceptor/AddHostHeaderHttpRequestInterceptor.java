package com.satispay.client.interceptor;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class AddHostHeaderHttpRequestInterceptor implements ClientHttpRequestInterceptor
{
  private final String host;

  public AddHostHeaderHttpRequestInterceptor(String host)
  {
    this.host = host;
  }

  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
    ClientHttpRequestExecution execution) throws IOException
  {
    request.getHeaders().add(HttpHeaders.HOST, host);
    return execution.execute(request, body);
  }
}
