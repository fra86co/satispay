package com.satispay.client.interceptor;

import com.satispay.client.BuildHeaderListFromRequest;
import com.satispay.signature.Header;
import com.satispay.signature.SignatureHeaderBuilder;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.List;

public class HttpSignatureHeaderInterceptor implements ClientHttpRequestInterceptor
{
  private final SignatureHeaderBuilder signatureHeaderBuilder;
  private final BuildHeaderListFromRequest buildHeaderListFromRequest;

  public HttpSignatureHeaderInterceptor(
    SignatureHeaderBuilder signatureHeaderBuilder,
    BuildHeaderListFromRequest buildHeaderListFromRequest)
  {
    this.signatureHeaderBuilder = signatureHeaderBuilder;
    this.buildHeaderListFromRequest = buildHeaderListFromRequest;
  }

  @Override
  public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes,
    ClientHttpRequestExecution clientHttpRequestExecution) throws IOException
  {
    List<Header> headers = buildHeaderListFromRequest.execute(httpRequest);

    httpRequest.getHeaders().add("Authorization", signatureHeaderBuilder.value(headers));

    return clientHttpRequestExecution.execute(httpRequest, bytes);
  }

}
