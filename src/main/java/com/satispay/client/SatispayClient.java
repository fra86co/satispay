package com.satispay.client;

import com.satispay.client.response.WallyServiceResponse;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

public class SatispayClient
{
  private String path;
  private RestTemplate restTemplate;

  public SatispayClient(String path, RestTemplate restTemplate)
  {
    this.path = path;
    this.restTemplate = restTemplate;
  }

  public WallyServiceResponse get()
  {

    return restTemplate
      .getForEntity(path, WallyServiceResponse.class).getBody();
  }

  public WallyServiceResponse post(String request)
  {
    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.CONTENT_TYPE, "application/json");
    HttpEntity<String> httpEntity = new HttpEntity<String>(request, headers);
    return restTemplate
      .exchange(path, HttpMethod.POST, httpEntity, WallyServiceResponse.class).getBody();
  }

  public WallyServiceResponse put(String request)
  {
    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.CONTENT_TYPE, "application/json");
    HttpEntity<String> httpEntity = new HttpEntity<String>(request, headers);
    return restTemplate
      .exchange(path, HttpMethod.PUT, httpEntity, WallyServiceResponse.class).getBody();
  }

  public WallyServiceResponse delete()
  {
    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.CONTENT_TYPE, "application/json");
    HttpEntity<String> httpEntity = new HttpEntity<String>(headers);
    return restTemplate
      .exchange(path, HttpMethod.DELETE, httpEntity, WallyServiceResponse.class).getBody();
  }
}
