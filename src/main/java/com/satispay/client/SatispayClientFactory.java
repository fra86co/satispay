package com.satispay.client;

import com.satispay.client.interceptor.AddDateHeaderHttpRequestInterceptor;
import com.satispay.client.interceptor.AddDigestHeaderHttpRequestInterceptor;
import com.satispay.client.interceptor.AddHostHeaderHttpRequestInterceptor;
import com.satispay.client.interceptor.HttpSignatureHeaderInterceptor;
import com.satispay.client.interceptor.RestTemplateLoggingInterceptor;
import com.satispay.signature.Algorithm;
import com.satispay.signature.BuildStringToSignFromHeaders;
import com.satispay.signature.BySigningStringSignatureBuilder;
import com.satispay.signature.SelectRequiredHeaders;
import com.satispay.signature.SignatureHeaderBuilder;
import com.satispay.signature.Signer;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Arrays;
import java.util.List;

public class SatispayClientFactory
{

  public SatispayClient create(String key, String host, String path, PrivateKey privateKey)
  {
    try
    {
      List<String> requiredHeaders = Arrays.asList("(request-target)", "date", "host", "digest");

      final HttpSignatureHeaderInterceptor httpSignatureHeaderInterceptor =
        new HttpSignatureHeaderInterceptor(
          new SignatureHeaderBuilder(
            key,
            new BySigningStringSignatureBuilder(
              new SelectRequiredHeaders(requiredHeaders),
              new BuildStringToSignFromHeaders(),
              new Signer(
                privateKey,
                Signature.getInstance(Algorithm.RSA.javaSecurityName))),
            Algorithm.RSA.code,
            requiredHeaders
          ),
          new BuildHeaderListFromRequest());

      RestTemplate restTemplate = new RestTemplateBuilder()
        .rootUri("https://" + host)
        .additionalInterceptors(new AddDateHeaderHttpRequestInterceptor())
        .additionalInterceptors(new AddHostHeaderHttpRequestInterceptor(host))
        .additionalInterceptors(new AddDigestHeaderHttpRequestInterceptor())
        .additionalInterceptors(httpSignatureHeaderInterceptor)
        .additionalInterceptors(new RestTemplateLoggingInterceptor())
        .build();

      return new SatispayClient(path, restTemplate);
    }
    catch (NoSuchAlgorithmException e)
    {
      throw new RuntimeException("Exception on client creation", e);
    }
  }

}
