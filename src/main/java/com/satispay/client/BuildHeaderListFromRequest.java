package com.satispay.client;

import com.satispay.signature.Header;
import com.satispay.signature.RequestTargetHeader;

import org.springframework.http.HttpRequest;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class BuildHeaderListFromRequest
{
  public List<Header> execute(HttpRequest httpRequest)
  {
    final List<Header> requestHeaders = httpRequest.getHeaders().entrySet().stream()
                                                   .map(e -> new Header(e.getKey(), e.getValue().get(0)))
                                                   .collect(Collectors.toList());
    Header requestTargetHeader = new RequestTargetHeader(httpRequest.getMethodValue(), httpRequest.getURI());

    List<Header> headers = new LinkedList<>();
    headers.add(requestTargetHeader);
    headers.addAll(requestHeaders);

    return headers;
  }
}
