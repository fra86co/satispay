package com.satispay.signature;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

public class Signer
{

  private static final Charset UTF_8 = StandardCharsets.UTF_8;

  private final PrivateKey privateKey;
  private final Signature signature;

  public Signer(PrivateKey privateKey, Signature instance)
  {
    this.privateKey = privateKey;
    this.signature = instance;
  }

  public String sign(String stringToSign)
  {
    try
    {
      signature.initSign(privateKey);
      signature.update(stringToSign.getBytes(UTF_8));
      final byte[] signedBytes = signature.sign();

      return new String(Base64.getEncoder().encode(signedBytes), UTF_8);
    }
    catch (InvalidKeyException | SignatureException e)
    {
      throw new RuntimeException("Exception during string signing", e);
    }
  }

}
