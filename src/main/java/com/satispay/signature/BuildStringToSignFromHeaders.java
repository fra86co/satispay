package com.satispay.signature;

import java.util.List;

public class BuildStringToSignFromHeaders
{
  public String execute(List<Header> headers)
  {
    StringBuilder sb = new StringBuilder();

    headers.forEach(it ->
      sb.append(it.name.toLowerCase())
        .append(": ")
        .append(it.value.trim())
        .append("\n"));

    final String result = sb.toString();
    return result.substring(0, result.length() - 1);
  }
}
