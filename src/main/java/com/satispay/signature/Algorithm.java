package com.satispay.signature;

public enum Algorithm
{
  RSA("RSA", "rsa-sha256", "SHA256withRSA");

  public final String id;
  public final String code;
  public final String javaSecurityName;

  Algorithm(String id, String code, String javaSecurityName)
  {
    this.id = id;
    this.code = code;
    this.javaSecurityName = javaSecurityName;
  }
}
