package com.satispay.signature;

import java.util.List;
import java.util.stream.Collectors;

public class SelectRequiredHeaders
{
  private final List<String> requiredHeaders;

  public SelectRequiredHeaders(List<String> asList)
  {
    requiredHeaders = asList;
  }

  public List<Header> execute(List<Header> headers)
  {
    if (headers.isEmpty())
    {
      throw new IllegalArgumentException("Empty header list");
    }

    if (requiredHeaders.isEmpty())
    {
      return headers.stream().filter(Header::isDateHeader).collect(Collectors.toList());
    }

    return requiredHeaders.stream()
                          .flatMap(required -> headers.stream().filter(it -> it.name.equalsIgnoreCase(required)))
                          .collect(Collectors.toList());

  }

}
