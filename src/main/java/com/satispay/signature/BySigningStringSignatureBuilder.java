package com.satispay.signature;

import java.util.List;

public class BySigningStringSignatureBuilder implements SignatureBuilder
{

  private final SelectRequiredHeaders selectRequiredHeaders;
  private final BuildStringToSignFromHeaders buildStringToSignFromHeaders;
  private final Signer signer;

  public BySigningStringSignatureBuilder(
    SelectRequiredHeaders selectRequiredHeaders,
    BuildStringToSignFromHeaders buildStringToSignFromHeaders,
    Signer signer)
  {
    this.selectRequiredHeaders = selectRequiredHeaders;
    this.buildStringToSignFromHeaders = buildStringToSignFromHeaders;
    this.signer = signer;
  }

  @Override
  public String build(List<Header> headers)
  {
    final List<Header> requiredHeaders = selectRequiredHeaders.execute(headers);
    final String stringToSign = buildStringToSignFromHeaders.execute(requiredHeaders);

    return signer.sign(stringToSign);
  }
}
