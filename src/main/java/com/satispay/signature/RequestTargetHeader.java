package com.satispay.signature;

import java.net.URI;

public class RequestTargetHeader extends Header
{
  public RequestTargetHeader(String methodValue, URI uri)
  {
    super("(request-target)", methodValue.toLowerCase() + " " + path(uri));
  }

  private static String path(URI uri)
  {
    final String uriString = uri.toString();
    final String uriWithoutHost = uriString.substring(uriString.indexOf("//") + 2);
    return uriWithoutHost.substring(uriWithoutHost.indexOf("/"));
  }
}
