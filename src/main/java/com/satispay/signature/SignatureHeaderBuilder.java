package com.satispay.signature;

import java.util.List;

public class SignatureHeaderBuilder
{
  private final String key;
  private String algorithm;
  private final List<String> requiredHeaders;
  private SignatureBuilder signatureBuilder;

  public SignatureHeaderBuilder(String key, SignatureBuilder signatureBuilder,
    String algorithm, List<String> requiredHeaders)
  {
    this.key = key;
    this.signatureBuilder = signatureBuilder;
    this.algorithm = algorithm;
    this.requiredHeaders = requiredHeaders;
  }

  public String value(List<Header> headers)
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Signature ");
    sb.append("keyId=\"").append(key).append("\", ");
    sb.append("algorithm=\"").append(algorithm).append("\", ");
    if (!this.requiredHeaders.isEmpty())
    {
      sb.append("headers=\"").append(String.join(" ", requiredHeaders)).append("\", ");
    }

    String signature = signatureBuilder.build(headers);

    sb.append("signature=\"").append(signature).append("\"");

    return sb.toString();

  }

}
