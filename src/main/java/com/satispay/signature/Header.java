package com.satispay.signature;

public class Header
{
  public final String name;
  public final String value;

  public Header(String name, String value)
  {
    this.name = name;
    this.value = value;
  }

  public boolean isDateHeader()
  {
    return name.equals("Date");
  }
}
