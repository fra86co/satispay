package com.satispay.signature;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

public class PKCS8EncodedPrivateKeyLoader implements PrivateKeyLoader
{
  @Override
  public PrivateKey load(String resource)
  {
    try
    {
      final PemReader pemReader = new PemReader(new FileReader(new File(getClass().getResource(resource).toURI())));
      PemObject pem = pemReader.readPemObject();
      final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pem.getContent());
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      return keyFactory.generatePrivate(keySpec);
    }
    catch (URISyntaxException | IOException | NoSuchAlgorithmException | InvalidKeySpecException e)
    {
      throw new RuntimeException("Exception loading private key", e);
    }
  }
}
