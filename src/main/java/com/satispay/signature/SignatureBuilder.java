package com.satispay.signature;

import java.util.List;

public interface SignatureBuilder
{
  String build(List<Header> headers);
}
