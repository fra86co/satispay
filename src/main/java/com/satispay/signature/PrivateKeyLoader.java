package com.satispay.signature;

import java.security.PrivateKey;

public interface PrivateKeyLoader
{
  PrivateKey load(String resource);
}
