package com.satispay;

import com.satispay.client.SatispayClient;
import com.satispay.client.SatispayClientFactory;
import com.satispay.client.response.WallyServiceResponse;
import com.satispay.signature.PKCS8EncodedPrivateKeyLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PrivateKey;

public class Main
{
  private static Logger logger = LoggerFactory.getLogger(Main.class);


  public static void main(String[] args) throws Exception
  {

    final String host = "staging.authservices.satispay.com";
    final String path = "/wally-services/protocol/tests/signature";

    final PrivateKey privateKey = new PKCS8EncodedPrivateKeyLoader().load("/satispay_private_key.txt");
    SatispayClient satispayClient = new SatispayClientFactory()
      .create("signature-test-66289", host, path, privateKey);

    logger.info("Calling get");
    final WallyServiceResponse wallyServiceResponse = satispayClient.get();
    logger.info("Get response: "+wallyServiceResponse);

    logger.info("Calling post");
    final WallyServiceResponse postResponse = satispayClient.post("");
    logger.info("Post response: "+postResponse);

    logger.info("Calling put");
    final WallyServiceResponse putResponse = satispayClient.put("");
    logger.info("put response: "+putResponse);

    logger.info("Calling delete");
    final WallyServiceResponse deleteResponse = satispayClient.delete();
    logger.info("delete response: "+putResponse);

  }
}
