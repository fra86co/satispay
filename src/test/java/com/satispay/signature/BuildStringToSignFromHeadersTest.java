package com.satispay.signature;

import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BuildStringToSignFromHeadersTest
{

  @Test
  public void onlyOneHeader()
  {
    Header header = new Header("Date", "Tue, 05 Mar 2019 08:17:00 GMT");

    final List<Header> headers = asList(header);

    BuildStringToSignFromHeaders underTest = new BuildStringToSignFromHeaders();

    final String stringToSign = underTest.execute(headers);

    assertThat(stringToSign,
      is("date: Tue, 05 Mar 2019 08:17:00 GMT"));
  }

  @Test
  public void moreHeaders()
  {
    Header requestTarget = new Header("(request-target)", "get /wally-services/protocol/tests/signature");
    Header dateHeader = new Header("Date", "Tue, 05 Mar 2019 08:17:00 GMT");
    Header hostHeader = new Header("Host", "staging.authservices.satispay.com");

    BuildStringToSignFromHeaders underTest = new BuildStringToSignFromHeaders();

    final String stringToSign = underTest.execute(asList(requestTarget, dateHeader, hostHeader));

    assertThat(stringToSign,
      is(
        "(request-target): get /wally-services/protocol/tests/signature\n" +
          "date: Tue, 05 Mar 2019 08:17:00 GMT\n" +
          "host: staging.authservices.satispay.com"));
  }

}