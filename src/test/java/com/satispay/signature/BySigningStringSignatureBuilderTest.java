package com.satispay.signature;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Rule;
import org.junit.Test;

import java.util.LinkedList;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BySigningStringSignatureBuilderTest
{

  @Rule
  public JUnitRuleMockery context = new JUnitRuleMockery()
  {{
    setImposteriser(ClassImposteriser.INSTANCE);
  }};

  private final SelectRequiredHeaders selectRequiredHeaders = context.mock(SelectRequiredHeaders.class);
  private final BuildStringToSignFromHeaders buildStringToSignFromHeaders = context.mock(BuildStringToSignFromHeaders.class);
  private final Signer signer = context.mock(Signer.class);

  @Test
  public void happyPath()
  {
    final LinkedList<Header> headers = new LinkedList<>();
    final LinkedList<Header> filteredHeaders = new LinkedList<>();

    context.checking(new Expectations(){{

      allowing(selectRequiredHeaders).execute(headers);
      will(returnValue(filteredHeaders));

      allowing(buildStringToSignFromHeaders).execute(filteredHeaders);
      will(returnValue("string to sign"));

      allowing(signer).sign("string to sign");
      will(returnValue("a_signature"));
    }});

    BySigningStringSignatureBuilder underTest = new BySigningStringSignatureBuilder(
      selectRequiredHeaders,
      buildStringToSignFromHeaders,
      signer);

    final String signature = underTest.build(headers);

    assertThat(signature, is("a_signature"));
  }
}