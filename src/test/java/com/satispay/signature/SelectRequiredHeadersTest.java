package com.satispay.signature;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SelectRequiredHeadersTest
{

  @Test(expected = IllegalArgumentException.class)
  public void noHeaders()
  {
    SelectRequiredHeaders underTest = new SelectRequiredHeaders(
      asList()
    );

    List<Header> headerList = underTest.execute(Collections.emptyList());

  }

  @Test
  public void onlyOneHeader()
  {
    SelectRequiredHeaders underTest = new SelectRequiredHeaders(
      asList()
    );

    List<Header> headerList = underTest.execute(asList(new Header("Date", "a date")));

    assertThat(headerList, hasSize(1));
    assertThat(headerList.get(0).name, is("Date"));
  }

  @Test
  public void headersSortedByRequiredHeaders()
  {
    SelectRequiredHeaders underTest = new SelectRequiredHeaders(
      asList("(request-target)", "date", "host")
    );

    List<Header> headerList = underTest.execute(asList(
      new Header("Date", "a date"),
      new Header("Host", "an host"),
    new Header("(request-target)", "a value")
    ));

    assertThat(headerList, hasSize(3));
    assertThat(headerList.get(0).name, is("(request-target)"));
    assertThat(headerList.get(0).value, is("a value"));
    assertThat(headerList.get(1).name, is("Date"));
    assertThat(headerList.get(1).value, is("a date"));
    assertThat(headerList.get(2).name, is("Host"));
    assertThat(headerList.get(2).value, is("an host"));
  }

  @Test
  public void onlyRequiredHeader()
  {
    SelectRequiredHeaders underTest = new SelectRequiredHeaders(
      asList("(request-target)", "date", "host")
    );

    List<Header> headerList = underTest.execute(asList(
      new Header("Date", "a date"),
      new Header("an_other", "a value"),
      new Header("Host", "an host"),
      new Header("(request-target)", "a value"),
      new Header("another one again", "a value")
    ));

    assertThat(headerList, hasSize(3));
    assertThat(headerList.get(0).name, is("(request-target)"));
    assertThat(headerList.get(0).value, is("a value"));
    assertThat(headerList.get(1).name, is("Date"));
    assertThat(headerList.get(1).value, is("a date"));
    assertThat(headerList.get(2).name, is("Host"));
    assertThat(headerList.get(2).value, is("an host"));
  }
}