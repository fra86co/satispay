package com.satispay.signature;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Base64;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class IetfCaseTest
{
  @Test
  public void defaultTest() throws Exception
  {

    String signingString = "date: Thu, 05 Jan 2012 21:31:40 GMT";

    final URI privateKeyFileURI = PKCS8EncodedPrivateKeyLoader.class.getResource("/private_key.txt").toURI();

    final PemReader pemReader = new PemReader(
      new FileReader(new File(privateKeyFileURI)));
    PemObject pem = pemReader.readPemObject();
    byte[] keyBytes = pem.getContent();

    final PrivateKey privateKey1 = RSA.privateKeyFromPKCS1(keyBytes);
    System.out.println("privateKey1 = " + privateKey1);

    final Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
    sha256withRSA.initSign(privateKey1);
    byte[] bytes = signingString.getBytes("UTF-8");
    sha256withRSA.update(bytes);
    final byte[] signedString = sha256withRSA.sign();

    String actualSignature = new String(Base64.getEncoder().encode(signedString), "UTF-8");

    String expectedSignature =
      new String(
        Files.readAllBytes(Paths.get(PKCS8EncodedPrivateKeyLoader.class.getResource("/signature_default.txt").toURI())),
        StandardCharsets.UTF_8);

    assertThat(actualSignature, is(expectedSignature));
  }

  @Test
  public void basicTest() throws Exception
  {

    String signingString =
      "(request-target): post /foo?param=value&pet=dog\n" +
        "host: example.com\n" +
        "date: Sun, 05 Jan 2014 21:31:40 GMT";

    final URI privateKeyFileURI = PKCS8EncodedPrivateKeyLoader.class.getResource("/private_key.txt").toURI();

    final PemReader pemReader = new PemReader(
      new FileReader(new File(privateKeyFileURI)));
    PemObject pem = pemReader.readPemObject();
    byte[] keyBytes = pem.getContent();

    final PrivateKey privateKey1 = RSA.privateKeyFromPKCS1(keyBytes);
    System.out.println("privateKey1 = " + privateKey1);

    final Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
    sha256withRSA.initSign(privateKey1);
    byte[] bytes = signingString.getBytes("UTF-8");
    sha256withRSA.update(bytes);
    final byte[] signedString = sha256withRSA.sign();

    String actualSignature = new String(Base64.getEncoder().encode(signedString), "UTF-8");

    String expectedSignature =
      new String(
        Files.readAllBytes(Paths.get(PKCS8EncodedPrivateKeyLoader.class.getResource("/signature_basic.txt").toURI())),
        StandardCharsets.UTF_8);

    assertThat(actualSignature, is(expectedSignature));
  }

  @Test
  public void allHeaderTest() throws Exception
  {

    String signingString =
      "(request-target): post /foo?param=value&pet=dog\n" +
        "host: example.com\n" +
        "date: Sun, 05 Jan 2014 21:31:40 GMT\n" +
        "content-type: application/json\n" +
        "digest: SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE=\n" +
        "content-length: 18";

    final URI privateKeyFileURI = PKCS8EncodedPrivateKeyLoader.class.getResource("/private_key.txt").toURI();

    final PemReader pemReader = new PemReader(
      new FileReader(new File(privateKeyFileURI)));
    PemObject pem = pemReader.readPemObject();
    byte[] keyBytes = pem.getContent();

    final PrivateKey privateKey1 = RSA.privateKeyFromPKCS1(keyBytes);
    System.out.println("privateKey1 = " + privateKey1);

    final Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
    sha256withRSA.initSign(privateKey1);
    byte[] bytes = signingString.getBytes("UTF-8");
    sha256withRSA.update(bytes);
    final byte[] signedString = sha256withRSA.sign();

    String actualSignature = new String(Base64.getEncoder().encode(signedString), "UTF-8");

    String expectedSignature =
      new String(
        Files
          .readAllBytes(Paths.get(PKCS8EncodedPrivateKeyLoader.class.getResource("/signature_allheader.txt").toURI())),
        StandardCharsets.UTF_8);

    assertThat(actualSignature, is(expectedSignature));
  }

}
