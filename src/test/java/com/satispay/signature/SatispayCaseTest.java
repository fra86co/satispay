package com.satispay.signature;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SatispayCaseTest
{

  @Test
  public void defaultTest_simple() throws Exception
  {
    String stringToSign =
      new BuildStringToSignFromHeaders().execute(Arrays.asList(
        new Header("(request-target)", "get /wally-services/protocol/tests/signature"),
        new Header("Date", "Tue, 05 Mar 2019 08:17:00 GMT"),
        new Header("Host", "staging.authservices.satispay.com")
      ));

    final PrivateKey privateKey = new PKCS8EncodedPrivateKeyLoader().load("/satispay_private_key.txt");
    final Signer signer = new Signer(privateKey, Signature.getInstance("SHA256withRSA"));
    String actualSignature = signer.sign(stringToSign);

    String expectedSignature =
      new String(
        Files
          .readAllBytes(Paths.get(PKCS8EncodedPrivateKeyLoader.class.getResource("/satispay_signature.txt").toURI())),
        StandardCharsets.UTF_8);

    assertThat(actualSignature, is(expectedSignature));
  }

}
