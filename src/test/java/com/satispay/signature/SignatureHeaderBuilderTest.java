package com.satispay.signature;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;

public class SignatureHeaderBuilderTest
{

  @Rule
  public JUnitRuleMockery context = new JUnitRuleMockery();

  private SignatureBuilder signatureBuilder = context.mock(SignatureBuilder.class);

  @Test
  public void defaultTest()
  {

    context.checking(new Expectations()
    {{
      allowing(signatureBuilder).build(emptyList());
      will(returnValue("a_signature"));
    }});

    SignatureHeaderBuilder signatureHeaderBuilder = new SignatureHeaderBuilder("Test", signatureBuilder, "rsa-sha256",
      emptyList());

    String signature = signatureHeaderBuilder.value(emptyList());

    Assert.assertThat(signature,
      is(
        "Signature keyId=\"Test\", algorithm=\"rsa-sha256\", signature=\"a_signature\""));
  }

  @Test
  public void basicTest()
  {

    context.checking(new Expectations()
    {{
      allowing(signatureBuilder).build(emptyList());
      will(returnValue("a_signature"));
    }});

    SignatureHeaderBuilder signatureHeaderBuilder = new SignatureHeaderBuilder("Test",
      signatureBuilder, "rsa-sha256", Arrays.asList("(request-target)", "host", "date"));

    String signature = signatureHeaderBuilder.value(emptyList());

    Assert.assertThat(signature,
      is(
        "Signature keyId=\"Test\", algorithm=\"rsa-sha256\", headers=\"(request-target) host date\", signature=\"a_signature\""));
  }

}
