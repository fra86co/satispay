package com.satispay.client;

import com.satispay.client.response.WallyServiceResponse;
import com.satispay.signature.PKCS8EncodedPrivateKeyLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;

import static org.hamcrest.CoreMatchers.is;

public class SatispayClientIT
{

  private SatispayClient satispayClient;

  @Before
  public void setUp() throws Exception
  {
    final String host = "staging.authservices.satispay.com";
    final String path = "/wally-services/protocol/tests/signature";

    final PrivateKey privateKey = new PKCS8EncodedPrivateKeyLoader().load("/satispay_private_key.txt");
    satispayClient =
      new SatispayClientFactory().create("signature-test-66289", host, path, privateKey);
  }

  @Test
  public void get()
  {
    WallyServiceResponse responseEntity = satispayClient.get();

    Assert.assertThat(responseEntity.authentication_key.role, is("DEVICE"));
    Assert.assertThat(responseEntity.authentication_key.enable, is(true));
  }

  @Test
  public void post()
  {
    WallyServiceResponse responseEntity = satispayClient.post(
      "{\n" +
        "  \"hello\": \"satispay\"\n" +
        "}");

    Assert.assertThat(responseEntity.authentication_key.role, is("DEVICE"));
    Assert.assertThat(responseEntity.authentication_key.enable, is(true));
  }

  @Test
  public void put()
  {
    WallyServiceResponse responseEntity = satispayClient.put(
      "{\n" +
        "  \"hello\": \"satispay\"\n" +
        "}");

    Assert.assertThat(responseEntity.authentication_key.role, is("DEVICE"));
    Assert.assertThat(responseEntity.authentication_key.enable, is(true));
  }

  @Test
  public void delete()
  {
    WallyServiceResponse responseEntity = satispayClient.delete();

    Assert.assertThat(responseEntity.authentication_key.role, is("DEVICE"));
    Assert.assertThat(responseEntity.authentication_key.enable, is(true));
  }

}
